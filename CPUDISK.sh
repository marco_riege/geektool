myCPU=`top -l 1 | awk '/CPU usage/ {print $3}' | sed s/%//`
myCPU=`echo "tmp=$myCPU; tmp /= 1; tmp" | bc`

typeset -i b=9
echo "CPU Usage    \c"
while [ $b -lt $myCPU ]
do
	echo "\033[1;37m▇\033[0m\c"
	b=`expr $b + 10`
done
#echo "\033[1;39m█\033[0m\c"
while [ $b -lt 99 ]
do
	echo "\033[2;30m▇\033[0m\c"

	b=`expr $b + 10`
done
echo "	$myCPU%\c"

echo "\r"
unset myCPU
unset b

if [ -f /tmp/disks.lst ]
then
rm /tmp/disks.lst
fi 
#getting disks..due to better handling with awk it creates a file
df -H | grep -vE '^Filesystem|tmpfs|cdrom|devfs|map|disk2|localhost' | awk '{ print $1 " " $5 }' >> /tmp/disks.lst
#how many disks do we have?
count=`wc -l /tmp/disks.lst|awk '{print $1}'`
for ((i=1;i <= $count;i++))
do
currname=`awk -v i=$i 'NR==i' /tmp/disks.lst|awk '{print $1}'`
echo "$currname   \c"
currp=`awk -v i=$i 'NR==i' /tmp/disks.lst|awk '{print $2}'|cut -d'%' -f1`
typeset -i a=9
while [ $a -lt $currp ]
do
echo "\033[1;37m▇\033[0m\c"
a=`expr $a + 10`
done
#echo "\033[1;39m█\033[0m\c"
while [ $a -lt 99 ]
do
echo "\033[2;30m▇\033[0m\c"
a=`expr $a + 10`
done
echo "	$currp%\c"
echo "\r"
done

unset count
unset i
unset currname
unset currp
unset a