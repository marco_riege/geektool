<?php
$Location = "Berlin, Germany";

$BASE_URL = "http://query.yahooapis.com/v1/public/yql";
$yql_query = 'select * from weather.forecast where woeid in (select woeid from geo.places(1) where text="'.$Location.'") and u="c"';
$yql_query_url = $BASE_URL . "?q=" . urlencode($yql_query) . "&format=json";
// Make call with cURL
$session = curl_init($yql_query_url);
curl_setopt($session, CURLOPT_RETURNTRANSFER,true);
$json = curl_exec($session);
// Convert JSON to PHP object
$phpObj =  json_decode($json,true);
//var_dump($phpObj);
$forcast = $phpObj['query']['results']['channel']['item']['forecast'];

foreach ($forcast as $daily) {
    echo $daily['day'].' '.$daily['low'].'° - '.$daily['high'].'° - '.$daily['text']."\r";
}  
?>